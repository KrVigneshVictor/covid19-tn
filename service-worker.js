// Update cache names any time any of the cached files change.
const CACHE_NAME = 'static-cache-v8';

// Add list of files to cache here.
const FILES_TO_CACHE = [
    'offline.html',
    'tamil-nadu.svg',
    'install.js',
    'assets/css/jquery.dataTables.min.css',
    'assets/css/materialize.min.css',
    'assets/css/responsive.dataTables.css',
    'assets/css/style.css',
    'assets/js/d3.v4.min.js',
    'assets/js/dataTables.responsive.js',
    'assets/js/jquery.dataTables.min.js',
    'assets/js/jquery-3.4.1.min.js',
    'assets/js/jquery.nav.js',
    'assets/js/materialize.min.js',
];

self.addEventListener('install', (evt) => {
	// console.log('[ServiceWorker] Install');
	// Precache static resources here.
	evt.waitUntil(
		caches.open(CACHE_NAME).then((cache) => {
			// console.log('[ServiceWorker] Pre-caching offline page');
			return cache.addAll(FILES_TO_CACHE);
		})
	);
	self.skipWaiting();
});

self.addEventListener('activate', (evt) => {
	// console.log('[ServiceWorker] Activate');
	// Remove previous cached data from disk.
	evt.waitUntil(
		caches.keys().then((keyList) => {
			return Promise.all(keyList.map((key) => {
				if (key !== CACHE_NAME) {
					// console.log('[ServiceWorker] Removing old cache', key);
					return caches.delete(key);
				}
			}));
		})
	);

	self.clients.claim();
});


self.addEventListener('fetch', (evt) => {
	// console.log('[ServiceWorker] Fetch', evt.request.url);
	// Add fetch event handler here.
	if (evt.request.mode !== 'navigate') {
		// Not a page navigation, bail.
		return;
	}
	evt.respondWith(
		fetch(evt.request)
		.catch(() => {
			return caches.open(CACHE_NAME)
				.then((cache) => {
					return cache.match('offline.html');
				});
		})
	);
});
